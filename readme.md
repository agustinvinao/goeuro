## Getting started

To get you started you need to install dependencies

```
npm install
```

This will install angular libraries and karma test framework libraries.

## Running tests

In order to run tests you need to start karma

```
karma start
```

## Running the application

To see the aplication working you only need to open `index.html`, this is a quite
simple application and we dont need an http server to provide any file, all the 
content we need is loaded in the index file.