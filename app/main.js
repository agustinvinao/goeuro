(function(){
  'use strict';

  angular
    .module(
      'RepositoriesList',
      [
        'RLComponents',
        'RLSrevices'
      ]
    );

})();