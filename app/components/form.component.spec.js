describe('Component: githubRepositoriesForm', function(){
  beforeEach(module('RepositoriesList'));

  var element, scope, controller;

  describe('unit tests', function(){
    beforeEach(inject(function($rootScope, $componentController){
      scope       = $rootScope.$new();
      controller  = $componentController('githubRepositoriesForm', {$scope: scope});
    }));
    it('onInit first state', function(){
      controller.$onInit();
      expect(controller.username).toEqual('');
    });
  });
  
  describe('e2e', function(){
    beforeEach(inject(function($rootScope, $compile){
      scope   = $rootScope.$new();
      element = angular.element('<github-repositories-form></github-repositories-form>');
      element = $compile(element)(scope);
      scope.$apply();
    }));
    
    it('has an empty username', function(){
      var input = element.find('input');
      expect(input.val()).toBe('');
    });

    it('submit button is disabled', function(){
      var button = element.find('button');
      expect(button.attr('disabled')).toBeTruthy();
    });
  });
});