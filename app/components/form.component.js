(function(){
  'use strict';

  angular
    .module('RLComponents')
    .component('githubRepositoriesForm', {
      template: `
<form novalidate
      ng-submit="$ctrl.submit()"
      name="$ctrl.githubForm"
      >
  <div class="pull-left"> 
    <div class="pull-left">Username:</div> 
    <div class="pull-left">
      <input type="text"
        name="username"
        ng-model="$ctrl.username"
        ng-model-options="{ debounce: 300 }"
        ng-minlength="4"
        required
        />
      <div class="error-description"
          ng-messages="$ctrl.githubForm.username.$error"
          ng-if="$ctrl.githubForm.username.$touched && $ctrl.githubForm.username.$invalid"
          >
        <div ng-message="required">Can't be empty</div>
        <div ng-message="minlength">Minimum 4 characters</div>
      </div>
    </div>
  </div>
  <button type="submit"
          ng-disabled="$ctrl.githubForm.$invalid"
          class="pull-left"
          >Submit</button>
</form>

<div class="clearfix"></div>

<repositories items="$ctrl.repositories"></repositories>
<div ng-show="$ctrl.error" class="error-description">{{::$ctrl.message}}</div>
`,
      controller: ['$log', 'api', function($log, api){
        $log.debug('GithubRepositoriesForm - Controller')

        var $ctrl     = this;

        $ctrl.$onInit = onInit; 
        $ctrl.submit  = submit;

        function onInit(){
          $ctrl.username = '';
        }

        function submit(){
          $log.debug('GithubRepositoriesForm - Controller - SUBMIT')
          api.repos({username: $ctrl.username})
             .$promise
             .then(repositoriesSuccess, respositoriesError);
        }

        function repositoriesSuccess(repositories){
          $ctrl.repositories = repositories;
        }

        function respositoriesError(response){
          $ctrl.error  =response;
          switch(response.status){
            case 404:
              $ctrl.message = "User not found"
              break;
            default:
              $ctrl.message = "Some error has occurred or API doesn't respond"
              break
          }
        }
      }]
    });
})();