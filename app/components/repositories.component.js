(function(){
  'use strict';

  angular
    .module('RLComponents')
    .component('repositories', {
      bindings: {
        items: '<'
      },
      template: `
<div ng-show="$ctrl.items.length > 0">
  <ul>
    <li ng-repeat="item in $ctrl.items">
      <repository item="item"></repository>
    </li>
  </ul>
</div>
<h4 ng-show="$ctrl.items.length === 0">Github user has no repos</h4>
`,
      controller: ['$log', function($log){
        $log.debug('repositories - Controller');
      }]
    });
})();