(function(){
  'use strict';

  angular
    .module('RLComponents')
    .component('repository', {
      bindings: {
        item: '<'
      },
      template: `
<a ng-href="{{::$ctrl.item.html_url}}" target="_blank">{{::$ctrl.item.name}}</a>
`,
      controller: ['$log', function($log){
        $log.debug('repositories - Controller')

        var $ctrl = this;
      }]
    });
})();