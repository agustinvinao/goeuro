describe('Component: repository', function(){
  beforeEach(module('RepositoriesList'));

  var element, scope, controller, item;

  describe('unit tests', function(){
    beforeEach(inject(function($rootScope, $componentController){
      scope       = $rootScope.$new();
      item        = { name: 'test' };
      controller  = $componentController('repository', {$scope: scope}, {item: item});
    }));
    it('onInit first state', function(){
      expect(controller.item).toEqual(item);
    });
  });
  
  describe('e2e', function(){
    beforeEach(inject(function($rootScope, $compile){
      scope       = $rootScope.$new();
      scope.item  = {
        name: 'test',
        html_url: 'test2'
      }; 
      element = angular.element('<repository item="item"></repository>');
      element = $compile(element)(scope);
      scope.$apply();
    }));
    
    it('pending', function(){
      var a = element.find('a');
      expect(a.text()).toBe(scope.item.name);
      expect(a.attr('href')).toBe(scope.item.html_url);
    });
  });
});