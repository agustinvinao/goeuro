describe('Component: repositories', function(){
  beforeEach(module('RepositoriesList'));

  var element, scope, controller;

  describe('unit tests', function(){
    beforeEach(inject(function($rootScope, $componentController){
      scope       = $rootScope.$new();
      controller  = $componentController('repositories', {$scope: scope}, {items: []});
    }));
    it('onInit first state', function(){
      expect(controller.items).toBeDefined();
      expect(controller.items.length).toBe(0);
    });
  });
  
  describe('e2e', function(){
    describe('no items', function(){
      beforeEach(inject(function($rootScope, $compile){
        scope   = $rootScope.$new();
        scope.items = [];
        element = angular.element('<repositories items="items"></repositories>');
        element = $compile(element)(scope);
        scope.$apply();
      }));
      
      it('show message with no repos', function(){
        var h4 = element.find('h4'),
        div_ul = element.find('div');
        expect(div_ul.hasClass('ng-hide')).toBe(true);
        expect(h4.hasClass('ng-hide')).toBe(false);
      });
    });
  
    describe('with items', function(){
      beforeEach(inject(function($rootScope, $compile){
        scope   = $rootScope.$new();
        scope.items = [
          {name: 'test', html_url: 'test'},
          {name: 'test1', html_url: 'test1'}
        ];
        element = angular.element('<repositories items="items"></repositories>');
        element = $compile(element)(scope);
        scope.$apply();
      }));
      
      it('show message with no repos', function(){
        var h4 = element.find('h4'),
        div_ul = element.find('div'),
         ul_li = element.find('li');
        expect(div_ul.hasClass('ng-hide')).toBe(false);
        expect(h4.hasClass('ng-hide')).toBe(true);
        expect(ul_li.length).toBe(2);
      });
    });
  });
});